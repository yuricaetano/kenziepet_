from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .serializers import CreateAnimalSerializer
from .models import Animal, Group, Characteristic

class Animals(APIView):
    def get(self, request):
        queryset = Animal.objects.all()
        serializer = CreateAnimalSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    def post(self, request):
        serializer = CreateAnimalSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        data = serializer.data
        group = data.pop('group')
        characteristic_set = data.pop('characteristic_set')
        group = Group.objects.get_or_create(**group)     
        animal_instance = Animal.objects.create(**data, group=group[0])
        for item in characteristic_set:
            characteristic = Characteristic.objects.get_or_create(**item)
            animal_instance.characteristic_set.add(characteristic[0])
        result = CreateAnimalSerializer(animal_instance)
        return Response(result.data, status=status.HTTP_201_CREATED)

class GetOneAnimal(APIView):
    def get(self, request, animal_id):
        animal = get_object_or_404(Animal, id=animal_id)
        serializer = CreateAnimalSerializer(animal)
        return Response(serializer.data)

    def delete(self, request, animal_id):
        animal = get_object_or_404(Animal, id=animal_id)
        animal.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

