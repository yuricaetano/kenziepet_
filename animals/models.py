from django.db import models

# 1 -> N group and animal

class Group(models.Model):
    name = models.CharField(max_length=255)
    scientific_name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

# N -> N Animal e Characteristic

class Characteristic(models.Model):
    characteristic = models.CharField(max_length=255)
    def __str__(self):
        return self.characteristic


class Animal(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    weight = models.IntegerField()
    sex = models.CharField(max_length=255)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    characteristic_set = models.ManyToManyField(Characteristic, related_name='animal')
    
    def __str__(self):
        return self.name

