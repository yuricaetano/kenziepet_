from django.urls import path
from .views import Animals, GetOneAnimal

urlpatterns = [
    path('animals/', Animals.as_view()),
    path('animals/<int:animal_id>/', GetOneAnimal.as_view()),
]
