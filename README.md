# Esta API permite você criar o perfil do seu pet, com as caracteristicas e grupo dele. 
O banco de dados é relacional e reaproveita os dados de caracteristicas e grupos caso se repitam.

## Como instalar e rodar no seu computador local

- Baixe o Python caso não tenha em https://www.python.org/downloads/.
- Baixe em um diretório esse repositorio clicando em "Download".
- Extraia.
- Pelo terminal dentro da pasta, execute: "pip install -r requirements.txt" sem aspas.
- Crie um banco de dados sqlite: "./manage.py migrate" sem aspas.
- Inicie o servidor: "./manage.py runserver" sem aspas.
- Utilize o insominia qual você quiser para fazer requests nas rotas abaixo.
- Para testar digite: "./manage.py test" sem aspas.
- Para gerar um report digite: "python manage.py test -v 2 &> report.txt"


Rotas:

## Criar um Animal

VERBO HTTP POST

http://127.0.0.1:8000/api/animals/


```
{
    "name":"Bidu",
    "age":1,
    "weight":30,
    "sex":"macho",
    "group":{
       "name":"ca1o",
       "scientific_name":"canis familiaris"
    },
    "characteristic_set":[
       {
          "characteristic":"peludo"
       },
       {
          "characteristic":"grande porte"
       }
    ]
 }
```
## Listar Animais

VERBO HTTP  GET 


http://127.0.0.1:8000/api/animals/

## Lista o Animal pelo ID

VERBO HTTP  GET 


http://127.0.0.1:8000/api/animal/1/

## Deletar o Animal pelo ID

VERBO HTTP DELETE


http://127.0.0.1:8000/api/animal/1/


